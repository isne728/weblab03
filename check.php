<?php
    //check forename statement
    $fname = $_POST["forename"];
    if(strlen($fname) == 0){ //check if forename blank or not
        echo "Must not be blank<br>";
    }elseif(strpos($fname, ' ')){ //check if forename contain space or not
        echo "Must not contain spaces<br>"; 
    }elseif(strlen($fname < 3)){ //check if forename less than 3 alphabet characters or not
        echo "Must have at least 3 alphabet characters<br>";
    }else{
        echo "This Forename is available<br><br>"; //forename can be use
    }

    //check surname statement
    $sname = $_POST["surname"];
    if(strlen($sname) == 0){ //check if surname blank or not
        echo "Must not be blank<br>";
    }elseif(strpos($sname, ' ')){ //check if surname contain space or not
        echo "Must not contain spaces<br>";
    }elseif(strlen($sname < 3)){ //check if surname more than 3 alphabet characters or not
        echo "Must have at least 3 alphabet characters<br>";
    }else{
        echo "This Surname is available<br><br>"; //surname can be use
    }

    //check username statement
    $uname = $_POST["username"];
    if(strlen($uname) < 5){ //check if username has at more than 5 alphabet charecters or not
        echo "Must have at least 5 characters<br>";
    }else{ 
        echo "This Username is available<br><br>"; //username can be use
    }

    //check password statement
    $pass = $_POST["password"];
    if(strlen($pass < 8)){ //check if password has less than 8 alphabet characters or not
        echo "Must have at least 8 alphabet characters<br>";
    }elseif(!preg_match('/[A-Z]/', $pass) && preg_match('/[a-z]/', $pass) && preg_match('/[0-9]/', $pass) && preg_match('/[!@#$%^&*()_+=-]/', $pass)){
        echo "Must containing both upper and lower case letters, numbers and symbols<br>";
    }else{
        echo "This Password is available<br><br>"; //password can be use
    }
 
    //check age statement
    $age = $_POST["age"];
    if($age < 18 || $age > 110){ //if under 18 or more than 110 years old
        echo "Under 18 or more than 110 years old is prohibited<br>";
    }else{ //between 18-110 years old
        echo "Welcome!<br>";
    }
    
    //check email statement
    $email = $_POST["email"];
    if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
        echo "Wrong format<br>";
    }else{ 
        echo "This Email is available<br><br>"; //email can be use
    }
?> 