<html>
    <head>
        <style>
            body{
                display: flex;
                justify-content: center;
                align-items: center;
                background-color: #E1E1E1;
            }
            #box{
                background-color: white;
                width: 50%;
                height: 60%;
                border-radius: 15px;
                display: flex;
                justify-content: center;
                align-items: center;
            }
            #submit{
                position: absolute;
                bottom: 150px;
                right: 370px;
            }
        </style>
    </head>
    <body>
        <div id = "box">
            <form action = "check.php" method = "post">
                <label for = "forename">Forename: </label>
                <input type = "text" id="forename" name="forename" placeholder="enter here"><br><br>
                <label for = "surname">Surname: </label>
                <input type = "text" id="surname" name="surname" placeholder="enter here"><br><br>
                <label for ="username">Username: </label>
                <input type = "text" id="username" name="username" placeholder="enter here"><br><br>
                <label for = "password">Password: </label>
                <input type = "text" id="password" name="password" placeholder="enter here"><br><br>
                <label for = "age">Age: </label>
                <input type ="number" id="age" name="age" placeholder="enter here"><br><br>
                <label for ="email">Email: </label>
                <input type = "email" id = "email" name ="email" placeholder="enter here"><br><br>
                <input type = "submit" id="submit" value="SUBMIT">
            </form>
        </div>

    
    </body>
    
</html>

